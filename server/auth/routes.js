var router = require('express').Router();
var verifyUser = require('./auth').verifyUser;
var controller = require('./controller');
var loginUser = require('./auth').loginUser;
var resetPassword = require('./auth').resetPassword;
var changePassword = require('./auth').changePassword;
var checkuser = require('./auth').checkuser;
var config = require('../config/config');
var loginAdmin = require('./auth').loginAdmin;
var addAdmin = require('./auth').addAdmin;

const LoginRegister=require('npm-login-register');
var dbName=config.dbname;
var tableName="users";
var dbType="mongodb";
var handler=new LoginRegister(dbName,tableName,dbType);

// before we send back a jwt, lets check
// the password and username match what is in the DB
//router.post('/signin', verifyUser(), controller.signin);
router.post('/login', loginUser());
router.post('/resetPassword', resetPassword());
router.post('/changePassword', checkuser(), changePassword());
router.post('/adminlogin', loginAdmin());
//router.post('/addAdmin', addAdmin()); 



module.exports = router;