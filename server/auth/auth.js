var jwt = require('jsonwebtoken');
var expressJwt = require('express-jwt');
var config = require('../config/config');
var checkToken = expressJwt({ secret: config.secrets.jwt });
var User = require('../api/user/userModel');
var Admin = require('./adminModel');
var crypto = require('crypto');

const LoginRegister=require('./controller');
var dbName=config.dbname;
var tableName="users";
adminTableName="admin"
var dbType="mongodb";
var handler=new LoginRegister(dbName,tableName,adminTableName,dbType);

// exports.checkAdmin= function(){
//   return function(req,res,next){
//     var adminId= req._id;
//     var token = req.token;
//     var email = req.username;
//     console.log("1"+adminId);
//     console.log("2"+token);
//     console.log("3"+email);
//     handler.checkAdmin(adminId,token,email,function(result){
//       if(result[0]==1)
//       {
//         console.log("admin is here");
//         //next();
//         //res.json({message: result[1]});
//       }
//       else
//       {
//         console.log("wrong");
//         res.json({message:result[1]});
//       }
//     });



//   }//
// }// end checkAdmin
exports.checkuser = function() {
  
  return function(req,res,next){
    var email = req.body.username;
    // console.log("printing username" + email );
    // handler.getToken(email , function(result, next){
    //   //console.log("getToken working");
    //   console.log(result);
    //   userId = result[1]._id;
    //   token= result[1].token;
    //   //console.log("printinf id   " + token )
    var userId = req.body.userId;
    var token = req.body.token;
   handler.checkToken(email,userId,token,function(result){
     if(result[0]==1)
      {
        next();
        //res.json({message: result[1]});
      }
      else
      {
        res.json({message:result[1]});
      }
      });
    // })

  } // end of return function
}//end of checkuser




exports.addAdmin = function ()
{
  return function(req, res, next) 
  {
  var newAdmin = new Admin(req.body); 
  handler.adminRegister(newAdmin,function(result){
     console.log(result);
      if(result[0]==1)
      {
        res.status(200).json(
          {
            message: "Created Admin",
            admin_data:result[1],
            code:"200"
        });
      }
      else
      {
        res.status(405).json(
          {
            message: result[1],
            code : "405"
          }); 
      }
  });


  }// end function

}; // end addAdmin


exports.loginAdmin = function() 
{
    return function(req, res, next) 
    {
      var email = req.body.email;
     // console.log(email);
      var password = req.body.password;
      //console.log(password);
      handler.adminLogin(email,password,function(result)
      {
      //console.log(result);
            if(result[0]==1){
            var resultArray=new Array();
           resultArray['code']=200;
           resultArray['user_data']=result[1];
            res.status(200).json({"code":200,"user_data":result[1]});
            }
            else
            {
              //res.send('Invalid token');
              res.status(401).json({message: result[1] , code : 401});
            }
      });
    };
}; // end loginAdmin






exports.loginUser = function() 
{
    return function(req, res, next) 
    {
      var email = req.body.username;
     // console.log(email);
      var password = req.body.password;
      //console.log(password);
      handler.userLogin(email,password,function(result)
      {
      //console.log(result);
            if(result[0]==1){
            var resultArray=new Array();
           resultArray['code']=200;
           resultArray['user_data']=result[1];
            res.status(200).json({"code":200,"user_data":result[1]});
            }
            else
            {
              //res.send('Invalid token');
              res.status(401).json({message: result[1] , code : 401});
            }
      });
    };
}; // end checkuser


exports.resetPassword= function() 
{
    return function(req, res ,next)
    {
      var email = req.body.username;
      if(email=="")
      {
        res.status(405).json({message: "Enter valid email for reset", code : "405"});
      }
      console.log(email);
      handler.forgotPassword(email,function(result)
      {
        console.log(result);
        if(result[0]==1)
            {
              res.json({message: result[1]});
            }
            else
            {
              res.json({message:result[1]});
            }
      });
    }
} // end reset password



exports.changePassword = function() {
  return function(req,res,next)
  {
    var userId;
    var token;
    var email = req.body.username;
    var oldpass = req.body.oldpassword;
    var newpass = req.body.newpassword
    if(email=="")
      {
        res.status(405).json({message: "Enter valid email for change", code : "405"});
      }
      if(oldpass=="")
      {
        res.status(405).json({message: "Enter old password for change", code : "405"});
      }
      if(newpass=="")
      {
        res.status(405).json({message: "Enter new password for change", code : "405"});
      }
      






    // handler.getToken(email , function(result,next){
    //   //console.log("getToken working");
    //   console.log(result);
    //   userId = result[1]._id;
    //   token= result[1].token;
    //   //console.log("printinf id   " + token )
    //   handler.checkToken(userId,token,function(result){
    //  if(result[0]==1)
    //   {
    //     res.json({message: result[1]});
    //   }
    //   else
    //   {
    //     res.json({message:result[1]});
    //   }
    //   });
    // })





    //   console.log(req.body._id);
    //   var userId="5b668dee1c6dee01ee8f6565";
    //   var token = "MlAZpXFXTa6b2i9c9EVnDaRGSUEeI1";
    // handler.checkToken(userId,token,function(result){
    //   console.log("working handle token")
    // console.log(result);
    //  if(result[0]==1)
    //   {
    //     res.json({message: result[1]});
    //   }
    //   else
    //   {
    //     res.json({message:result[1]});
    //   }
    //   });



    handler.changePassword(email,oldpass,newpass,function(result)
    {
      console.log(result);


      if(result[0]==1){
        var resultArray=new Array();
       resultArray['code']=200;
       resultArray['user_data']=result[1];
        res.status(200).json({"code":200,"user_data":result[1]});
        }
        else
        {
         // resultArray['code']=401;
           //resultArray['message']=result[1];
           res.status(200).json({"code":401,"message":result[1]});
          // res.status(401).json({code : 401,message: result[1]  });
        }





      // if(result[0]==1)
      // {
      //   res.json({message: result[1]});
      // }
      // else
      // {
      //   res.json({message:result[1]});
      // }
    });
  }
}//end change password

exports.decodeToken = function() {
  return function(req, res, next) {
    // make it optional to place token on query string
    // if it is, place it on the headers where it should be
    // so checkToken can see it. See follow the 'Bearer 034930493' format
    // so checkToken can see it and decode it
    if (req.query && req.query.hasOwnProperty('access_token')) {
      req.headers.authorization = 'Bearer ' + req.query.access_token;
    }

    // this will call next if token is valid
    // and send error if its not. It will attached
    // the decoded token to req.user
    checkToken(req, res, next);
  };
};

exports.getFreshUser = function() {
  return function(req, res, next) {
    User.findById(req.user._id)
      .then(function(user) {
        if (!user) {
          // if no user is found it was not
          // it was a valid JWT but didn't decode
          // to a real user in our DB. Either the user was deleted
          // since the client got the JWT, or
          // it was a JWT from some other source
          res.status(401).send('Unauthorized');
        } else {
          // update req.user with fresh user from
          // stale token data
          req.user = user;
          next();
        }
      }, function(err) {
        next(err);
      });
  }
};

exports.verifyUser = function() {
  return function(req, res, next) {
    var username = req.body.username;
    var password = req.body.password;

    // if no username or password then send
    if (!username || !password) {
      res.status(400).json({message : "Enter username and password"});
      return;
    }

    // look user up in the DB so we can check
    // if the passwords match for the username
    User.findOne({username: username})
      .then(function(user) {
        if (!user) {
          res.status(401).json({message : "No user exists with this username"});
        } else {
          // checking the passowords here
          if (!user.authenticate(password)) {
            res.status(401).json({message : "wrong password"});
          } else {
            // if everything is good,
            // then attach to req.user
            // and call next so the controller
            // can sign a token from the req.user._id
            req.user = user;
            next();
          }
        }
      }, function(err) {
        next(err);
      });
  };
};


// util method to sign tokens on signup
exports.signToken = function(id) {
/*    require('crypto').randomBytes(48, function(err, buffer) {
    var token = buffer.toString('hex');
    console.log(token);
    return token;
      });
*/

var token = crypto.randomBytes(64).toString('hex'); // random token generaor
return token;

/*  return jwt.sign(
    {_id: id},
    config.secrets.jwt,
    {expiresInMinutes: config.expireTime}  //working code of tokens
  );*/ // old working option...genertaing same token on every login
};
