var router = require('express').Router();

// api router will mount other routers
// for all our resources
router.use('/movie', require('./video/videoRoutes'));
router.use('/users', require('./user/userRoutes'));
router.use('/show', require('./show/showRoutes'));
router.use('/season', require('./season/seasonRoutes'));
//router.use('/episode', require('./episode/episodeRoutes'));


//router.use('/categories', require('./category/categoryRoutes'));
//router.use('/posts', require('./post/postRoutes'));

module.exports = router;
