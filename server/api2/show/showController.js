var Show = require('./showModel');
var router = require('express').Router();
var _ = require('lodash');
var Season = require('../season/seasonModel');

exports.params = function(req, res, next, id) {
    Show.findById(id)
    .populate('season_ids')
    .exec()
      .then(function(show) {
        if (!show) {
          next(new Error('No show with that id'));
        } else {
          req.show = show;
          next();
        }
      }, function(err) {
        next(err);
      });
  };



  exports.get = function(req, res, next) {
    Show.find({})
      .then(function(shows){
        //console.log(username);
        //var pass=req.body.password;\
        //var offset = req.body.c;
        //console.log(videos[offset].Name)
        res.json(shows);
      }, function(err){
        next(err);
      });
  };


  exports.getOne = function(req, res, next) {
    var show = req.show;
   //console.log(video.categories[0]);
    res.json(show);
  };
  



  exports.getc = function(req, res, next) {
    Show.find({})
      .then(function(shows){
        //console.log(username);
        //var pass=req.body.password;\
        var offset = req.body.index;
        //console.log(videos[offset].categories)
        res.json(shows[offset]);
      }, function(err){
        next(err);
      });
  };


  exports.delete = function(req, res, next) {
    req.show.remove(function(err, removed) {
      if (err) {
        next(err);
      } else {
        //console.log(removed.season_ids);
        //Season.find({show_id : removed._id},function(err, user){
        //Season.findById(removed.season_ids, function(err, user) {  // everytime you add new season then it's id will get added into
        //console.log(user);
        Season.remove({show_id : removed._id},function(err, remov)
      {
        console.log("removed");
      }); // this 3 line will delete those seasons which belongs to the show you are deleting
       // Show.update( {$push :{season_ids: season._id}}, //shows array of season_ids by this function
        // function(err) {
        // });
      //}); // show by find id ends here




        res.json(removed);
      }
    });
  };




  exports.update = function(req, res, next) {
    var show = req.show;
    //console.log(user);
  
    var update = req.body;
   
    // if(!update.password)  // active this block if you don't want some particular field should not be changed
    // {
    _.merge(show, update);
  
   show.save(function(err, saved) {
      if (err) {
        next(err);
      } else {
        console.log("changing db")
        res.json(saved);
      }
    })
  // }
  // else
  // {
  //   res.json({message : "you cannot change password"});
  // }
  };



  exports.post = function(req, res, next) {
    var newShow = new Show(req.body);
    newShow.save(function(err, show) {
      if(err) {
        next(err);
      }
      else{
      res.json(show);
        }
    });
  };