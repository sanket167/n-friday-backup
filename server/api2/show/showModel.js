var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ShowSchema = new Schema({
  Name: {
    type: String,
    trim: true,
    lowercase: true,
    required: 'Video name is required'
  }, // dont store the password as plain text
  year: {
    type: Number,
    required: true
  },

  No_of_seasons:{
      type:Number
  },
  categories:{
    type:Array
  },
  isLive:
  {
    type:Boolean
  },
  createOn:
  {
    type:Date
  },
  updateOn:
  {
    type:Date
  },
  season_ids:
  [{
    type: Schema.Types.ObjectId, ref: 'season'
  }]

});

module.exports = mongoose.model('show', ShowSchema);
