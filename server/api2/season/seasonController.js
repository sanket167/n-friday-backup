var Season = require('./seasonModel');
var router = require('express').Router();
var _ = require('lodash');
var Show = require('../show/showModel');


exports.params = function(req, res, next, id) {
  Season.findById(id)
  .populate('show_id')
  .exec()
      .then(function(season) {
        if (!season) {
          next(new Error('No Season with that id'));
        } else {
          req.season = season;
          next();
        }
      }, function(err) {
        next(err);
      });
  };



  exports.get = function(req, res, next) {
    Season.find({})
      .then(function(seasons){
        //console.log("going here")
        //console.log(username);
        //var pass=req.body.password;\
        //var offset = req.body.c;
        //console.log(videos[offset].Name)
        res.json(seasons);
      }, function(err){
        next(err);
      });
  };


  exports.getOne = function(req, res, next) {
    var season = req.season;
   //console.log(video.categories[0]);
    res.json(season);
  };
  



  exports.getc = function(req, res, next) {
    Season.find({})
    
      .then(function(seasons){
        //console.log(username);
        //var pass=req.body.password;\
        var offset = req.body.index;
        //console.log(videos[offset].categories)
        res.json(seasons[offset]);
      }, function(err){
        next(err);
      });
  };


  exports.delete = function(req, res, next) {
    req.season.remove(function(err, removed) {
      if (err) {
        next(err);
      } else {
        res.json(removed);
      }
    });
  };




  exports.update = function(req, res, next) {
    var season = req.season;
    //console.log(user);
  
    var update = req.body;
   
    // if(!update.password)  // active this block if you don't want some particular field should not be changed
    // {
    _.merge(season, update);
  
   season.save(function(err, saved) {
      if (err) {
        next(err);
      } else {
        console.log("changing db")
        res.json(saved);
      }
    })
  // }
  // else
  // {
  //   res.json({message : "you cannot change password"});
  // }
  };



  exports.post = function(req, res, next) {
    var newSeason = new Season(req.body);
    // var newShow = new Show();
    newSeason.save(function(err, season) {
      if(err) {
        next(err);
      }
      else{
        //console.log()
          Show.findById(season.show_id, function(err, user) {  // everytime you add new season then it's id will get added into
          console.log(user);
          user.update( {$push :{season_ids: season._id}}, //shows array of season_ids by this function
          function(err) {
          });
        }); // show by find id ends here
      res.json(season);
        }
    });
  };