var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SeasonSchema = new Schema({
  Name: {
    type: String,
    trim: true,
    lowercase: true,
    required: 'Video name is required'
  }, // dont store the password as plain text
  year: {
    type: Number,
    required: true
  },
  show_id:
  {
    type: Schema.Types.ObjectId, ref: 'show'
  },
  isLive:
  {
    type:Boolean
  },
  createOn:
  {
    type:Date
  },
  updateOn:
  {
    type:Date
  }

});

module.exports = mongoose.model('season', SeasonSchema);
