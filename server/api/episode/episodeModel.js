var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var EpisodeSchema = new Schema({
  Name: {
    type: String,
    trim: true,
    lowercase: true,
    required: 'Video name is required'
  }, // dont store the password as plain text
  year: {
    type: Number,
    required: true
  },
  season_id:
  {
    type: Schema.Types.ObjectId, ref: 'season'
  },
  createOn:
  {
    type:Date
  },
  updateOn:
  {
    type:Date
  },
  isLive:
  {
    type:Boolean,
    required:true,
    default:true
  },
  starcast:
  [{
    type:String,
    lowercase: true,
    trim: true
  }]

});

module.exports = mongoose.model('episode', EpisodeSchema);
