var router = require('express').Router();
var logger = require('../../util/logger');
var controller = require('./episodeController');
var Episode = require('./episodeModel');




router.param('id', controller.params);

router.route('/')
 .get(controller.get) // get call for getting a all video
 
router.route('/addepisode')
 .post(controller.post); // creating video

router.route('/getc')
    .post(controller.getc) // returns element of videos as per array index

router.route('/:id')
    .get(controller.getOne) // return video details by passing id 
    .delete(controller.delete) // deleting video with id
    .put(controller.update) //method for updating a video
    // it will give error if .update method is not defined in controller

module.exports = router; // it is very important line