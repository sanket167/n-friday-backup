var Episode = require('./episodeModel');
var router = require('express').Router();
var _ = require('lodash');
var Season = require('../season/seasonModel');


exports.params = function(req, res, next, id) {
  Episode.findById(id)
  .populate('show_id season_id')
  .exec()
      .then(function(episode) {
        if (!episode) {
          next(new Error('No Episode with that id'));
        } else {
          req.episode = episode;
          next();
        }
      }, function(err) {
        next(err);
      });
  };



  exports.get = function(req, res, next) {
    Episode.find({})
      .then(function(episodes){
        //console.log("going here")
        //console.log(username);
        //var pass=req.body.password;\
        //var offset = req.body.c;
        //console.log(videos[offset].Name)
        res.json(episodes);
      }, function(err){
        next(err);
      });
  };


  exports.getOne = function(req, res, next) {
    var episode = req.episode;
   //console.log(video.categories[0]);
    res.json(episode);
  };
  



  exports.getc = function(req, res, next) {
    Episode.find({})
    
      .then(function(episodes){
        //console.log(username);
        //var pass=req.body.password;\
        var offset = req.body.index;
        //console.log(videos[offset].categories)
        res.json(episodes[offset]);
      }, function(err){
        next(err);
      });
  };


  exports.delete = function(req, res, next) {
    var episode = req.episode;
    var episodeId=episode._id;
    var update = {isLive:"false"};
    _.merge(episode, update);
    episode.save(function(err, changed) {   //this function not deleting episode files just setting the isLive to false
      if (err) {
        next(err);
      } else {
            console.log("changing episodes to isLive False")
            res.json(changed); 
      }
    }) 





    // req.episode.remove(function(err, removed) {
    //   if (err) {
    //     next(err);
    //   } else {
    //     res.json(removed);
    //   }
    // });
  };




  exports.update = function(req, res, next) {
    var episode = req.episode;
    //console.log(user);
  
    var update = req.body;
   
    // if(!update.password)  // active this block if you don't want some particular field should not be changed
    // {
    _.merge(episode, update);
  
   episode.save(function(err, saved) {
      if (err) {
        next(err);
      } else {
        console.log("changing db")
        res.json(saved);
      }
    })
  // }
  // else
  // {
  //   res.json({message : "you cannot change password"});
  // }
  };



  exports.post = function(req, res, next) {
    var newEpisode = new Episode(req.body);
    // var newSeason = new Season();
    newEpisode.save(function(err, episode) {
      if(err) {
        next(err);
      }
      else{
        //console.log()
          Season.findById(episode.season_id, function(err, user) {  // everytime you add new episode then it's id will get added into season
          console.log(user);
          user.update( {$push :{episode_ids: episode._id}}, //seasons array of episode_ids by this function
          function(err) {
          });
        }); // season by find id ends here
      res.json(episode);
        }
    });
  };