var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SeasonSchema = new Schema({
  Name: {
    type: String,
    trim: true,
    lowercase: true,
    required: 'Video name is required'
  }, // dont store the password as plain text
  year: {
    type: Number,
    required: true
  },
  show_id:
  {
    type: Schema.Types.ObjectId, ref: 'show'
  },
  createOn:
  {
    type:Date
  },
  updateOn:
  {
    type:Date
  },
  isLive:
  {
    type:Boolean,
    required:true,
    default:true
  },
  episode_ids:
  [{
    type: Schema.Types.ObjectId, ref: 'episode'
  }],
  starcast:
  [{
    type:String,
    lowercase: true,
    trim: true
  }]

});

module.exports = mongoose.model('season', SeasonSchema);
