var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var VideoSchema = new Schema({
  Name: {
    type: String,
    trim: true,
    lowercase: true,
    required: 'Video name is required'
  }, // dont store the password as plain text
  year: {
    type: Number,
    required: true
  },
  path:
  {
    type:String
  },
  categories:{
    type:Array
  },
  isLive:
  {
    type:Boolean,
    required:true,
    default:true
  },
  image:
  {
    type:String
  },
  Rating:
  {
    type:Number
  },
  duration:
  {
    type:String
  },
  createdOn:
  {
    type:Date
  },
  starcast:
  [{
    type:String,
    lowercase: true,
    trim: true
  }]

});

module.exports = mongoose.model('video', VideoSchema);
