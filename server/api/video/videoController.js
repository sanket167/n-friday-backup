var Video = require('./videoModel');
var router = require('express').Router();
var _ = require('lodash');



exports.params = function(req, res, next, id) {
    Video.findById(id)
      .then(function(video) {
        if (!video) {
          next(new Error('No video with that id'));
        } else {
          req.video = video;
          next();
        }
      }, function(err) {
        next(err);
      });
  };



  exports.get = function(req, res, next) {
    Video.find({})
      .then(function(videos){
        //console.log(username);
        //var pass=req.body.password;\
        //var offset = req.body.c;
        //console.log(videos[offset].Name)
        res.json(videos);
      }, function(err){
        next(err);
      });
  };

  // exports.allDelete = function(req, res, next){
  //   Video.remove(function(err, remov)
  //   {
  //     //console.log("removed");
  //     res.status(200).json({"code":200,"removed_Data":remov});
  //    // res.json({message: "removed all videos"});
  //   });
  // };


  exports.getOne = function(req, res, next) {
    var video = req.video;
   //console.log(video.categories[0]);
    res.json(video);
  };
  



  exports.getc = function(req, res, next) {
    Video.find({})
      .then(function(videos){
        //console.log(username);
        //var pass=req.body.password;\
        var offset = req.body.index;
        //console.log(videos[offset].categories)
        res.json(videos[offset]);
      }, function(err){
        next(err);
      });
  };


  exports.delete = function(req, res, next) {
    var video = req.video;
    var update = {isLive:"false"};
    _.merge(video, update);
   video.save(function(err, changed) {   //this function not deleting video files just setting the isLive to false
      if (err) {
        next(err);
      } else {
        console.log("changing video isLive to false")
        res.json(changed);
      }
    })  
    // req.video.remove(function(err, removed) {
    //   if (err) {
    //     next(err);
    //   } else {
    //     res.json(removed);
    //   }
    // });
  };




  exports.update = function(req, res, next) {
    var video = req.video;
    //console.log(user);
  
    var update = req.body;
   
    // if(!update.password)  // active this block if you don't want some particular field should not be changed
    // {
    _.merge(video, update);
  
   video.save(function(err, saved) {
      if (err) {
        next(err);
      } else {
        console.log("changing db")
        res.json(saved);
      }
    })
  // }
  // else
  // {
  //   res.json({message : "you cannot change password"});
  // }
  };



  exports.post = function(req, res, next) {
    var newVideo = new Video(req.body);
    var currentdate = new Date();
    newVideo['createdOn']=currentdate;
    newVideo.save(function(err, video) {
      if(err) {
        next(err);
      }
      else{
      res.json(video);
        }
    });
  };